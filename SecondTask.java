import java.util.Scanner;

public class SecondTask {
    public static void main(String[] args) {
        Scanner myscan = new Scanner(System.in);

        double temperature;
        double windSpeed;
        String isRaining;

        System.out.println("Enter the temperature");
        temperature = myscan.nextDouble();
        System.out.println("Enter the wind speed");
        windSpeed = myscan.nextDouble();
        System.out.println("if it's raining outside, enter \"yes\", otherwise enter \"no\"");
        isRaining = myscan.next();
        System.out.println(isRaining);
        if(isRaining.equals("yes")){
            System.out.println("It's better to stay at home today");
        }
        else if(isRaining.equals("no")) {
            if(windSpeed < 0){
                System.out.println("incorrect input");
            }
            else if(temperature - windSpeed * 4 <= -20) { // the wind at a speed of 1 mps reduces the temperature by 4
                System.out.println("It's better to stay at home today");
            }
            else {
                System.out.println("going for a walk is a great idea for today");
            }
        }
        else{
            System.out.println("incorrect input");
        }
    }
}
